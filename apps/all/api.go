package all

import (
	// 注册所有HTTP服务模块, 暴露给框架HTTP服务器加载
	_ "gitee.com/gongpengABC/keyauth-gp/apps/book/api"
	_ "gitee.com/gongpengABC/keyauth-gp/apps/policy/api"
	_ "gitee.com/gongpengABC/keyauth-gp/apps/role/api"
	_ "gitee.com/gongpengABC/keyauth-gp/apps/token/api"
	_ "gitee.com/gongpengABC/keyauth-gp/apps/user/api"
)
