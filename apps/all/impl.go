package all

import (
	_ "gitee.com/gongpengABC/keyauth-gp/apps/audit/impl"
	// 注册所有GRPC服务模块, 暴露给框架GRPC服务器加载, 注意 导入有先后顺序
	_ "gitee.com/gongpengABC/keyauth-gp/apps/book/impl"
	_ "gitee.com/gongpengABC/keyauth-gp/apps/endpoint/impl"
	_ "gitee.com/gongpengABC/keyauth-gp/apps/policy/impl"
	_ "gitee.com/gongpengABC/keyauth-gp/apps/role/impl"
	_ "gitee.com/gongpengABC/keyauth-gp/apps/token/impl"
	_ "gitee.com/gongpengABC/keyauth-gp/apps/user/impl"
)
