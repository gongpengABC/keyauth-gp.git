package impl

import (
	"context"
	"gitee.com/gongpengABC/keyauth-gp/apps/endpoint"
	"github.com/infraboard/mcube/exception"
)

func (s *service) save(ctx context.Context, set *endpoint.EndpointSet) error {
	if _, err := s.col.InsertMany(ctx, set.ToDocs()); err != nil {
		return exception.NewInternalServerError("inserted service %s endpoint document error, %s",
			set.Service, err)
	}
	return nil
}
