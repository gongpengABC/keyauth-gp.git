package endpoint

const (
	AppName = "endpoint"
)

func (s *EndpointSet) ToDocs() (docs []interface{}) {
	for i := range s.Endpoints {
		docs = append(docs, s.Endpoints[i])
	}
	return
}
func NewRegistryResponse() *RegistryResponse {
	return &RegistryResponse{}
}

func NewRegistryEndpointRequest() *EndpointSet {
	return &EndpointSet{
		Service: "123",
	}
}
