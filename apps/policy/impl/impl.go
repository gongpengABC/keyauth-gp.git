package impl

import (
	"gitee.com/gongpengABC/keyauth-gp/apps/policy"
	"gitee.com/gongpengABC/keyauth-gp/apps/role"
	"gitee.com/gongpengABC/keyauth-gp/conf"
	"github.com/infraboard/mcube/app"
	"google.golang.org/grpc"

	"github.com/infraboard/mcube/logger"
	"go.mongodb.org/mongo-driver/mongo"

	"github.com/infraboard/mcube/logger/zap"
)

var (
	svr = &service{}
)

type service struct {
	col  *mongo.Collection
	log  logger.Logger
	role role.Service // 添加角色对象
	policy.UnimplementedRPCServer
}

func (s *service) Config() error {
	// 依赖MongoDB的DB对象

	db, err := conf.C().Mongo.GetDB()
	if err != nil {
		return err
	}

	// 获取一个Collection对象, 通过Collection对象 来进行CRUD
	s.col = db.Collection(s.Name())
	s.log = zap.L().Named(s.Name())
	s.role = app.GetInternalApp(role.AppName).(role.Service) // 需要因为role模块:policy中使用到role模块
	return nil
}

func (s *service) Name() string {
	return policy.AppName
}

func (s *service) Registry(server *grpc.Server) {
	policy.RegisterRPCServer(server, svr)
}

func init() {
	app.RegistryInternalApp(svr)
	app.RegistryGrpcApp(svr)
}
