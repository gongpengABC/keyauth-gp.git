package impl

import (
	"context"
	"gitee.com/gongpengABC/keyauth-gp/apps/policy"
	"gitee.com/gongpengABC/keyauth-gp/apps/role"
	"github.com/infraboard/mcube/exception"
)

func (s *service) ValidatePermission(ctx context.Context, req *policy.ValidatePermissionRequest) (
	*policy.Policy, error) {
	// 根据用户和命名空间-------> 找到该用户的授权策略
	// 由于使用分页, 只查询100条数据
	s.log.Debugf("found req:%s", req)
	query := policy.NewQueryPolicyRequest()
	query.Username = req.Username
	query.Namespace = req.Namespace
	query.Page.PageSize = 100
	s.log.Debugf("found query:%s", query)
	set, err := s.QueryPolicy(ctx, query) // 查询用户对应的策略(policy)-------> 获得策略列表
	s.log.Debugf("found set:%s", set)
	if err != nil {
		return nil, err
	}
	//获取用户角色，从策略中抽取出来
	roleNames := set.Roles()
	s.log.Debugf("found roles:%s", roleNames)
	// 通Role模块查询所有的Role对象
	queryRoleReq := role.NewQueryRoleRequestWithName(roleNames)
	queryRoleReq.Page.PageSize = 100
	roles, err := s.role.QueryRole(ctx, queryRoleReq)
	if err != nil {
		return nil, err
	}
	// 根据Role判断用户是否具有权限: req.Service, req.Resource, req.Action
	hasPerm, role := roles.HasPermission(role.NewPermissionRequest(req.Service, req.Resource, req.Action))
	if !hasPerm { // 没有权限，返回一个异常
		return nil, exception.NewPermissionDeny("not permission access service %s resource %s action %s",
			req.Service,
			req.Resource,
			req.Action,
		)
	}

	p := set.GetPolicyByRole(role.Spec.Name)
	return p, nil
}
func (s *service) QueryPolicy(ctx context.Context, req *policy.QueryPolicyRequest) (
	*policy.PolicySet, error) {
	query := newQueryPolicyRequest(req)
	return s.query(ctx, query)
}

func (s *service) CreatePolicy(ctx context.Context, req *policy.CreatePolicyRequest) (
	*policy.Policy, error) {
	ins, err := policy.NewPolicy(req)
	if err != nil {
		return nil, exception.NewBadRequest("validate create book error, %s", err)
	}

	if err := s.save(ctx, ins); err != nil {
		return nil, err
	}

	return ins, err
}
