package impl

import (
	"context"
	"gitee.com/gongpengABC/keyauth-gp/apps/role"
	"github.com/infraboard/mcube/exception"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Save Object
// Save Object
func (s *service) save(ctx context.Context, ins *role.Role) error {
	// s.col.InsertMany()
	if _, err := s.col.InsertOne(ctx, ins); err != nil {
		return exception.NewInternalServerError("inserted role(%s) document error, %s",
			ins.Spec.Name, err)
	}
	return nil
}

// 把QueryReq --> MongoDB Options
type queryRequest struct {
	*role.QueryRoleRequest
}

func newQueryRequest(r *role.QueryRoleRequest) *queryRequest {
	return &queryRequest{
		r,
	}
}
func (r *queryRequest) FindOptions() *options.FindOptions {
	pageSize := int64(r.Page.PageSize)
	skip := int64(r.Page.PageSize) * int64(r.Page.PageNumber-1)

	opt := &options.FindOptions{
		Sort: bson.D{
			{Key: "create_at", Value: -1},
		},
		Limit: &pageSize,
		Skip:  &skip,
	}
	return opt
}
func (r *queryRequest) FindFilter() bson.M {
	filter := bson.M{}
	if len(r.RoleNames) > 0 {
		filter["spec.name"] = bson.M{"$in": r.RoleNames} //spec.name是否在r.RoleNames中
	}
	return filter
}
func (s *service) query(ctx context.Context, req *queryRequest) (*role.RoleSet, error) {
	resp, err := s.col.Find(ctx, req.FindFilter(), req.FindOptions())
	if err != nil {
		return nil, exception.NewInternalServerError("find policy error, error is %s", err)
	}
	set := role.NewRoleSet()
	for resp.Next(ctx) {
		ins := role.NewDefaultRole()
		if err := resp.Decode(ins); err != nil {
			return nil, exception.NewInternalServerError("decode policy error, error is %s", err)
		}
		set.Add(ins)
	}
	// count
	count, err := s.col.CountDocuments(ctx, req.FindFilter())
	if err != nil {
		return nil, exception.NewInternalServerError("get policy count error, error is %s", err)
	}
	set.Total = count
	return set, nil
}
