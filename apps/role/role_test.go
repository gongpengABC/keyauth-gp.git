package role_test

import (
	"gitee.com/gongpengABC/keyauth-gp/apps/role"
	"testing"
)

func TestHasPermission(t *testing.T) {
	set := role.NewRoleSet()
	r := &role.Role{

		Spec: &role.CreateRoleRequest{
			Permissions: []*role.Permission{
				{ // 对切片进行赋初值
					Service: "cmdb",
					Featrues: []*role.Featrue{
						{
							Resource: "secret",
							Action:   "list",
						},
						{
							Resource: "secret",
							Action:   "get",
						},
						{
							Resource: "secret",
							Action:   "create",
						},
					},
				},
			},
		},
	}
	set.Add(r)
	perm, role := set.HasPermission(&role.PermissionRequest{
		Service:  "cmdb",
		Resource: "secret",
		//Action:   "create",
		Action: "list",
	})
	t.Log(role)
	if perm != true {
		t.Fatal("has perm error")
	}
}
