package token

import (
	"fmt"
	"gitee.com/gongpengABC/keyauth-gp/apps/user"
	"gitee.com/gongpengABC/keyauth-gp/common/utils"
	"github.com/infraboard/mcube/exception"
	"time"
)

const (
	AppName = "token"
)

func NewDefaultToken() *Token {
	return &Token{
		Data: &IssueTokenRequest{}, // 因为这是一个地址，需要初始化

		Meta: map[string]string{},
	}
}
func NewIssueTokenRequest() *IssueTokenRequest {
	return &IssueTokenRequest{
		UserDomain: user.DefaultDomain,
	}
}
func NewValidateTokenRequest(s string) *ValidateTokenRequest {
	return &ValidateTokenRequest{
		AccessToken: s,
	}
}
func NewRevolkTokenRequest() *RevolkTokenRequest {
	return &RevolkTokenRequest{}
}

func (req *IssueTokenRequest) Validate() error {
	switch req.GranteType {
	case GranteType_PASSWORD:
		if req.Password == "" || req.UserName == "" {
			return fmt.Errorf("password grant required username and password")
		}
	}
	return nil
}
func NewToken(req *IssueTokenRequest, expiredDuration time.Duration) *Token {
	now := time.Now()
	expired := now.Add(expiredDuration)
	refrash := now.Add(expiredDuration * 5)
	return &Token{
		AccessToken: utils.MakeBearer(24),
		IssueAt:     now.UnixMilli(),

		Data:                  req,
		AccessTokenExpiredAt:  expired.UnixMilli(),
		RefreshToken:          utils.MakeBearer(24),
		RefreshTokenExpiredAt: refrash.UnixMilli(),
	}
}
func (t *Token) Validate() error {
	if time.Now().UnixMilli() > t.AccessTokenExpiredAt {
		return exception.NewAccessTokenExpired("access token expired")
	}
	return nil
}
func (t *Token) IsRefreshTokenExpired() bool {
	// 判断refresh Token过期没有
	// 是一个时间戳,
	//  now   expire
	if time.Now().UnixMilli() > t.RefreshTokenExpiredAt {
		return true
	}

	return false
}

// Extend 续约Token, 延长一个生命周期
func (t *Token) Extend(expiredDuration time.Duration) {
	now := time.Now()
	// Token 10
	expired := now.Add(expiredDuration)
	refresh := now.Add(expiredDuration * 5)

	t.AccessTokenExpiredAt = expired.UnixMilli()
	t.RefreshTokenExpiredAt = refresh.UnixMilli()
}
