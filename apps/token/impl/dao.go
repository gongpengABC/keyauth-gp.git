package impl

import (
	"context"
	"fmt"
	"gitee.com/gongpengABC/keyauth-gp/apps/token"
	"github.com/infraboard/mcube/exception"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func (s *impl) save(ctx context.Context, ins *token.Token) error {
	if _, err := s.col.InsertOne(ctx, ins); err != nil {
		return exception.NewInternalServerError("inserted token(%s) document error, %s",
			ins.AccessToken, err)
	}
	return nil
}
func (s *impl) get(ctx context.Context, accessToken string) (*token.Token, error) {
	filter := bson.M{"_id": accessToken}
	ins := token.NewDefaultToken()
	if err := s.col.FindOne(ctx, filter).Decode(ins); err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, exception.NewNotFound("access token %s not found", accessToken)
		}
		return nil, exception.NewInternalServerError("find access token %s error, %s", accessToken, err)
	}
	return ins, nil
}

func (s *impl) delete(ctx context.Context, ins *token.Token) error {
	if ins == nil || ins.AccessToken == "" {
		return fmt.Errorf("access token is nil")
	}
	// delete from access token where id = ?
	result, err := s.col.DeleteOne(ctx, bson.M{"_id": ins.AccessToken})
	if err != nil {
		return exception.NewInternalServerError("delete access token(%s) error, %s", ins.AccessToken, err)
	}
	if result.DeletedCount == 0 {
		return exception.NewNotFound("access token %s not found", ins.AccessToken)
	}
	return nil
}

// update 根据ins.AccessToken(id)更新，更新数据是ins
func (s *impl) update(ctx context.Context, ins *token.Token) error {
	if _, err := s.col.UpdateByID(ctx, ins.AccessToken, ins); err != nil {
		return exception.NewInternalServerError("update token(%s) document error, %s",
			ins.AccessToken, err)
	}
	return nil
}
