package user

import (
	"gitee.com/gongpengABC/keyauth-gp/common/utils"
	"github.com/go-playground/validator/v10"
	"github.com/infraboard/mcube/http/request"
	pb_request "github.com/infraboard/mcube/pb/request"
	"github.com/rs/xid"
	"net/http"
	"time"
)

const (
	AppName = "user"
)
const (
	DefaultDomain = "default"
)

var (
	validate = validator.New()
)

func (req *CreateUserRequest) Validate() error {
	return validate.Struct(req)
}

func NewUser(req *CreateUserRequest) *User {
	return &User{
		Id:       xid.New().String(),
		CreateAt: time.Now().UnixMilli(),
		Data:     req, // 因为data数据类型是地址，需要初始化
	}
}

func NewUserSet() *UserSet {
	return &UserSet{
		Items: []*User{},
	}
}
func NewDefaultUser() *User {
	return NewUser(NewCreateUserRequest())
}
func NewCreateUserRequest() *CreateUserRequest {
	return &CreateUserRequest{
		Domain: DefaultDomain,
	}
}

func (s *UserSet) Add(item *User) {
	s.Items = append(s.Items, item)
}

func NewQueryUserRequestFromHTTP(r *http.Request) *QueryUserRequest {
	qs := r.URL.Query()
	return &QueryUserRequest{
		Page:     request.NewPageRequestFromHTTP(r), //从分页中添加分页请求
		Keywords: qs.Get("keywords"),
	}
}

func NewPutUserRequest(id string) *UpdateUserRequest {
	return &UpdateUserRequest{
		Id:         id,
		UpdateMode: pb_request.UpdateMode_PUT,
		UpdateAt:   time.Now().UnixMilli(),
		Data:       NewCreateUserRequest(),
	}
}

func NewPatchUserRequest(id string) *UpdateUserRequest {
	return &UpdateUserRequest{
		Id:         id,
		UpdateMode: pb_request.UpdateMode_PATCH,
		UpdateAt:   time.Now().UnixMilli(),
		Data:       NewCreateUserRequest(),
	}
}

func NewDeleteUserRequestWithID(id string) *DeleteUserRequest {
	return &DeleteUserRequest{
		Id: id,
	}
}

//indexs := []mongo.IndexModel{ 索引是domain和name，可以进行查询
//		{
//			Keys: bsonx.Doc{
//				{Key: "data.domain", Value: bsonx.Int32(-1)},
//				{Key: "data.name", Value: bsonx.Int32(-1)},
//			},
//			Options: options.Index().SetUnique(true), //设置唯一键索引
//		},
//	}
// 查询user有两种方式

func NewDescribeUserRequestByName(domain, name string) *DescribeUserRequest {
	return &DescribeUserRequest{

		DescribeBy: DescribeBy_USER_NAME,

		Domain:   domain,
		UserName: name,
	}
}

func NewDescribeUserRequestById(id string) *DescribeUserRequest {
	return &DescribeUserRequest{
		DescribeBy: DescribeBy_USER_ID,
		UserId:     id,
	}
}
func (u *User) CheckPassword(passWord string) bool {
	return utils.CheckPasswordHash(passWord, u.Data.Password)
}
