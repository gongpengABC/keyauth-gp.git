package impl

import (
	"context"
	"gitee.com/gongpengABC/keyauth-gp/apps/user"
	"gitee.com/gongpengABC/keyauth-gp/conf"
	"github.com/infraboard/mcube/app"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
	"google.golang.org/grpc"
)

var (
	svr = &impl{}
)

type impl struct {
	col *mongo.Collection
	log logger.Logger
	user.UnimplementedServiceServer
}

func (s *impl) Name() string {
	return user.AppName
}
func (s *impl) Config() error {
	db, err := conf.C().Mongo.GetDB()
	if err != nil {
		return err
	}
	// 获取一个Collection对象, 通过Collection对象 来进行CRUD
	s.col = db.Collection(s.Name())
	s.log = zap.L().Named(s.Name())
	// 创建索引，避免一个用户创建多个账号
	indexs := []mongo.IndexModel{
		{
			Keys: bsonx.Doc{
				{Key: "data.domain", Value: bsonx.Int32(-1)},
				{Key: "data.name", Value: bsonx.Int32(-1)},
			},
			Options: options.Index().SetUnique(true), //设置唯一键索引
		},
	}

	_, err = s.col.Indexes().CreateMany(context.Background(), indexs)
	if err != nil {
		return err
	}
	return nil
}

/*
notify.RegisterRPCServer(server, svr) 是一个gRPC框架中的函数，
它的作用是将一个实现了指定gRPC服务接口的服务对象注册到gRPC服务端。
具体来说，它的参数 server 是一个 *grpc.Server 类型的对象，表示gRPC服务端，
参数 svr 则是一个实现了gRPC服务接口的对象，它需要满足gRPC服务定义中指定的所有接口方法。
当 notify.RegisterRPCServer(server, svr) 被调用时，gRPC框架会将 svr 中实现的服务方法注册到 server 中，
以便客户端可以通过gRPC协议与 svr 中的服务进行通信。具体而言，它会使用gRPC框架自动生成的代码创建并注册gRPC服务对象，
以便将来可以通过gRPC协议调用这些服务方法。
值得注意的是，notify.RegisterRPCServer(server, svr) 只是将服务对象注册到gRPC服务端，
并不会启动gRPC服务。要启动gRPC服务，您需要在调用notify.RegisterRPCServer(server, svr)
之后调用 server.Serve(listen)，其中 listen 是一个网络监听对象，用于监听客户端请求。
*/

func (s *impl) Registry(server *grpc.Server) {
	user.RegisterServiceServer(server, svr)
}
func init() {
	app.RegistryGrpcApp(svr)
}
