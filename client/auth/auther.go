package auth

import (
	"gitee.com/gongpengABC/keyauth-gp/apps/token"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
)

type KeyauthAuther struct {
	log  logger.Logger
	auth token.ServiceClient
}

func NewKeyauthAuther(auth token.ServiceClient) *KeyauthAuther {
	return &KeyauthAuther{
		auth: auth,
		log:  zap.L().Named("http.auther"),
	}
}
