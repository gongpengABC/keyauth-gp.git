package client

import (
	"fmt"
	"gitee.com/gongpengABC/keyauth-gp/apps/audit"
	"gitee.com/gongpengABC/keyauth-gp/apps/book"
	"gitee.com/gongpengABC/keyauth-gp/apps/endpoint"
	"gitee.com/gongpengABC/keyauth-gp/apps/policy"
	"gitee.com/gongpengABC/keyauth-gp/apps/role"
	"github.com/infraboard/mcenter/client/rpc"
	"github.com/infraboard/mcenter/client/rpc/auth"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"github.com/infraboard/mcenter/client/rpc/resolver"

	"gitee.com/gongpengABC/keyauth-gp/apps/token"
)

var (
	client *ClientSet
)

// SetGlobal todo
func SetGlobal(cli *ClientSet) {
	client = cli
}

// C Global
func C() *ClientSet {
	return client
}

// NewClient todo
// 传递注册中心的地址，在client里面传入
func NewClient(conf *rpc.Config) (*ClientSet, error) {
	zap.DevelopmentSetup()
	log := zap.L()

	// resolver 进行解析的时候 需要mcenter客户端实例已经初始化
	conn, err := grpc.Dial(
		// 127.0.0.1:18010 GRPC server端的地址
		// 基于服务发现  Dial to "passthrough://  dns://keyauth.org "mcenter://keyauth",
		fmt.Sprintf("%s://%s", resolver.Scheme, "keyauth"), // 注册中心服务的名字是:keyauth
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithPerRPCCredentials(auth.NewAuthentication(conf.ClientID, conf.ClientSecret)),
		grpc.WithBlock(),
	)
	if err != nil {
		return nil, err
	}

	return &ClientSet{
		conn: conn,
		log:  log,
	}, nil
}

// Client 客户端:包括所有的client
type ClientSet struct {
	conn *grpc.ClientConn
	log  logger.Logger
}

// Book服务的SDK
func (c *ClientSet) Book() book.ServiceClient {
	return book.NewServiceClient(c.conn)
}

// Token 的客户端的SDK。在中间件传输过程中，只使用了token.ServiceClient，我们从所有的client去除token的client
func (c *ClientSet) Token() token.ServiceClient {
	return token.NewServiceClient(c.conn)
}

// Endpoint 服务SDK
func (c *ClientSet) Endpoint() endpoint.RPCClient {
	return endpoint.NewRPCClient(c.conn)
}

func (c *ClientSet) Policy() policy.RPCClient {
	return policy.NewRPCClient(c.conn)
}
func (c *ClientSet) Role() role.RPCClient {
	return role.NewRPCClient(c.conn)
}
func (c *ClientSet) Audit() audit.RPCClient {
	return audit.NewRPCClient(c.conn)
}
