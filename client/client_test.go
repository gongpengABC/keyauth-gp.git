package client_test

import (
	"context"
	"fmt"
	"gitee.com/gongpengABC/keyauth-gp/apps/audit"
	"gitee.com/gongpengABC/keyauth-gp/apps/endpoint"
	"gitee.com/gongpengABC/keyauth-gp/apps/token"
	"gitee.com/gongpengABC/keyauth-gp/client"
	mcenter "github.com/infraboard/mcenter/client/rpc"
	"github.com/stretchr/testify/assert"
	"os"

	"testing"

	"gitee.com/gongpengABC/keyauth-gp/apps/policy"
)

// 执行测试用例，一定要启动主程序
func TestBookQuery(t *testing.T) {
	//should := assert.New(t)
	//
	//conf := client.NewDefaultConfig()
	//// 设置GRPC服务地址
	//// conf.SetAddress("127.0.0.1:8050")
	//// 携带认证信息
	//// conf.SetClientCredentials("secret_id", "secret_key")
	//

	should := assert.New(t)

	conf := mcenter.NewDefaultConfig()

	conf.Address = os.Getenv("MCENTER_ADDRESS")
	conf.ClientID = os.Getenv("MCENTER_CDMB_CLINET_ID") // 使用CMDB的客户端访问keyauth服务
	conf.ClientSecret = os.Getenv("MCENTER_CMDB_CLIENT_SECRET")
	//conf.Address = "127.0.0.1:18010"
	//conf.ClientID = "p2nMkW8FZGYLexZEDLtW1bOG"
	//conf.ClientSecret = "SgU0tlA2BJWfqHJyistxlAWM0DdwLIGj"

	// 传递Mcenter配置, 客户端通过Mcenter进行搜索，在NewClient函数中，提前写了一个keyauth参数，会生成一个用户中心的客户端
	keyauthClient, err := client.NewClient(conf) // 通过mcenter的参数keyauth找到keyauth服务
	// c.Token().ValidateToken() 使用SDK调用keyauth进行凭证的校验
	if should.NoError(err) {
		//resp, err := c.Book().QueryBook(
		//	context.Background(),
		//	book.NewQueryBookRequest(),
		//)
		//should.NoError(err)
		//fmt.Println(resp.Items)
		resp, err := keyauthClient.Token().ValidateToken( // 访问一个keyauth提供服务
			context.Background(),
			token.NewValidateTokenRequest("8my8nmoE8uhWHq05hF8miZbZ"),
		)
		should.NoError(err)
		fmt.Println(resp)
	}
}

// 测试鉴权
func TestRegistryEndpoint(t *testing.T) {
	should := assert.New(t)

	conf := mcenter.NewDefaultConfig()
	conf.Address = os.Getenv("MCENTER_ADDRESS")
	conf.ClientID = os.Getenv("MCENTER_CDMB_CLINET_ID")
	conf.ClientSecret = os.Getenv("MCENTER_CMDB_CLIENT_SECRET")

	// 传递Mcenter配置, 客户端通过Mcenter进行搜索, New一个用户中心的客户端
	keyauthClient, err := client.NewClient(conf)

	if should.NoError(err) {
		req := endpoint.NewRegistryEndpointRequest()
		req.Service = "cmdb"

		//req.Endpoints = []*endpoint.Endpoint{
		//	Resource: "Resource",
		//	Action:   "list",
		//	Path:     "/cmdb-gp/api/v1/Resource/search",
		//	Method:   "GET"
		//}

		p, err := keyauthClient.Endpoint().RegistryEndpoint(context.TODO(), req)
		if err != nil {
			t.Fatal(err)
		}
		t.Log(p)
	}
}

/*  TestValidatePermission 输出结果
{
	"_id": ObjectId("6435e6dddbd6cda53baf962e"),
	"username": "member",
	"when": NumberLong("1681254109350"),
	"service": "",
	"resource": "secret",
	"action": "get",
	"url": "",
	"request": "",
	"response": "",
	"cost": NumberLong("0"),
	"status_code": NumberLong("0"),
	"user_agent": "",
	"remote_ip": "",
	"meta": null
}
*/
// 测试鉴权
func TestValidatePermission(t *testing.T) {
	should := assert.New(t)

	conf := mcenter.NewDefaultConfig()
	conf.Address = os.Getenv("MCENTER_ADDRESS")
	conf.ClientID = os.Getenv("MCENTER_CDMB_CLINET_ID")
	conf.ClientSecret = os.Getenv("MCENTER_CMDB_CLIENT_SECRET")

	// 传递Mcenter配置, 客户端通过Mcenter进行搜索, New一个用户中心的客户端
	keyauthClient, err := client.NewClient(conf)
	if should.NoError(err) {
		req := policy.NewValidatePermissionRequest()
		req.Username = "member"
		req.Service = "cmdb"
		req.Resource = "secret"
		//req.Action = "get"
		req.Action = "create"

		p, err := keyauthClient.Policy().ValidatePermission(context.TODO(), req)
		if err != nil {
			t.Fatal(err)
		}
		t.Log(p)
	}
}
func TestAuditOperate(t *testing.T) {
	should := assert.New(t)

	conf := mcenter.NewDefaultConfig()
	conf.Address = os.Getenv("MCENTER_ADDRESS")
	conf.ClientID = os.Getenv("MCENTER_CDMB_CLINET_ID")
	conf.ClientSecret = os.Getenv("MCENTER_CMDB_CLIENT_SECRET")
	AuditClient, err := client.NewClient(conf)
	if should.NoError(err) {
		req := audit.NewOperateLog("member", "secret", "get")
		p, err := AuditClient.Audit().AuditOperate(context.TODO(), req)
		if err != nil {
			t.Fatal(err)
		}
		t.Log(p)
	}
}
func init() {
	// 提前加载好 mcenter客户端, resolver需要使用
	err := mcenter.LoadClientFromEnv()
	if err != nil {
		panic(err)
	}
}
