package auth

import (
	"gitee.com/gongpengABC/keyauth-gp/apps/audit"
	"gitee.com/gongpengABC/keyauth-gp/apps/policy"
	"gitee.com/gongpengABC/keyauth-gp/apps/token"
	"gitee.com/gongpengABC/keyauth-gp/client"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
)

type KeyauthAuther struct {
	log         logger.Logger
	auth        token.ServiceClient
	perm        policy.RPCClient
	audit       audit.RPCClient
	serviceName string
}

//func NewKeyauthAuther(auth token.ServiceClient) *KeyauthAuther {
//	return &KeyauthAuther{
//		auth: auth,
//	}
//}
func NewKeyauthAuther(client *client.ClientSet, serviceName string) *KeyauthAuther {
	return &KeyauthAuther{
		log:         zap.L().Named("http.auther"),
		auth:        client.Token(),
		perm:        client.Policy(),
		audit:       client.Audit(),
		serviceName: serviceName,
	}
}
