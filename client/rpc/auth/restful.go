package auth

import (
	"gitee.com/gongpengABC/keyauth-gp/apps/audit"
	"gitee.com/gongpengABC/keyauth-gp/apps/policy"
	"gitee.com/gongpengABC/keyauth-gp/apps/token"
	"gitee.com/gongpengABC/keyauth-gp/common/utils"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/exception"
	"github.com/infraboard/mcube/http/label"
	"time"

	"github.com/infraboard/mcube/http/response"

	"github.com/infraboard/mcube/http/request"
)

func (a *KeyauthAuther) RestfulAuthHandlerFunc(
	req *restful.Request,
	resp *restful.Response,
	chain *restful.FilterChain,
) {
	// 1. 能不能获取路由装饰信息
	meta := req.SelectedRoute().Metadata()
	a.log.Debug("keyauth_gp信息", meta)

	// 获取meta信息, get, 判断是否开启认证
	var isAuthEnable bool
	if authV, ok := meta[label.Auth]; ok {
		switch v := authV.(type) {
		case bool:
			isAuthEnable = v
		case string:
			isAuthEnable = v == "true"
		}
	}
	if isAuthEnable {
		// 处理Request对象
		// *restful.Request

		// 1. 认证中间件, 需要获取Token
		tkStr := utils.GetToken(req.Request)

		// 2. 到用户中心验证token合法性, 依赖用户中心的Grpc Client
		validateReq := token.NewValidateTokenRequest(tkStr)
		tk, err := a.auth.ValidateToken(req.Request.Context(), validateReq)
		if err != nil {
			response.Failed(resp.ResponseWriter, err)
			return
		}

		// 处理完成后 需要直接中断返回
		// *restful.Response
		// resp.Header()
		// resp.Write()

		// 如果处理ok需要把一些中间结果 然后后面的请求也能方式, 需要把结果保存中 上下文中
		// 原生 c.Request.Context()
		// 如何:  context Get
		// 认证完成后, 我们需要用户名称或者其他信息 传递下去

		// Set Context
		req.SetAttribute("token", tk)
		// req.Attribute("token").(*token.Token)
	}

	// 获取meta信息, get, 判断是否开启鉴权
	var isPermEnable bool
	if authV, ok := meta[label.Permission]; ok {
		switch v := authV.(type) {
		case bool:
			isPermEnable = v
		case string:
			isPermEnable = v == "true"
		}
	}

	// 认证后，才能鉴权,进行策略Policy的查询
	if isAuthEnable && isPermEnable {
		tk := req.Attribute("token").(*token.Token)
		permReq := policy.NewValidatePermissionRequest()
		permReq.Service = a.serviceName
		permReq.Username = tk.Data.UserName

		if meta != nil {
			if v, ok := meta[label.Resource]; ok {
				permReq.Resource, _ = v.(string)
			}
			if v, ok := meta[label.Action]; ok {
				permReq.Action, _ = v.(string)
			}
		}

		_, err := a.perm.ValidatePermission(req.Request.Context(), permReq)
		if err != nil {
			response.Failed(resp.ResponseWriter, exception.NewPermissionDeny(err.Error()))
			return
		}
	}
	// 获取meta信息, get, 判断是否开启鉴权
	var isAuditEnable bool
	if authV, ok := meta[label.Auth]; ok {
		switch v := authV.(type) { // var x interface{}
		case bool:
			isAuthEnable = v
		case string:
			isAuthEnable = v == "true"
		}
	}
	start := time.Now()
	chain.ProcessFilter(req, resp)
	cost := time.Now().Sub(start).Microseconds()
	// 如果有记录Response需求的话，需要在Process才有进行的
	// 认证后，才能审计
	if isAuthEnable && isAuditEnable {
		tk := req.Attribute("token").(*token.Token)
		auditReq := audit.NewOperateLog(tk.Data.UserName, "", "")
		auditReq.Service = a.serviceName
		auditReq.Url = req.Request.URL.String()
		auditReq.Cost = cost
		auditReq.StatusCode = int64(resp.StatusCode())
		auditReq.UserAgent = req.Request.UserAgent()
		auditReq.RemoteIp = request.GetRemoteIP(req.Request)

		if meta != nil {
			if v, ok := meta[label.Resource]; ok {
				auditReq.Resource, _ = v.(string)
			}
			if v, ok := meta[label.Action]; ok {
				auditReq.Action, _ = v.(string)
			}
		}
		_, err := a.audit.AuditOperate(req.Request.Context(), auditReq)
		if err != nil {
			a.log.Warnf("audit operate failed, %s", err)
			return
		}
	}

	//// 认证中间件的实现
	////1.获取token
	//tkStr := utils.GetToken(req.Request)
	////2.通过客户端，验证token
	//validateReq := token.NewValidateTokenRequest(tkStr)
	//tk, err := a.auth.ValidateToken(req.Request.Context(), validateReq)
	//if err != nil {
	//	response.Failed(resp.ResponseWriter, err)
	//	return
	//}
	//// Set Context
	//req.SetAttribute("token", tk)
	//// req.Attribute("token").(*token.Token) 下面一个环节可以直接使用

	// chain 用于将请求flow下去

}
