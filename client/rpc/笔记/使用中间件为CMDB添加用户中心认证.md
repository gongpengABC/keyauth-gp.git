## 为什么把认证服务放在keyauth服务
其他的服务也是需要进行认证，也需要用户认证，所以放在keyauth也比较容易维护
```shell
192:mcenter_gp gongpeng$ make init
2023-04-04T21:27:15.797+0800    INFO    [INIT]  cmd/start.go:153        log level: debug
2023-04-04T21:27:15.818+0800    INFO    [init]  cmd/init.go:50  init app keyauth success, client_id: o3TffZFsPIx0JPCBEOMejYEw, client_secret: 8yOThSqSE5A3brIBF4NnQRQIFCQPF4ne
2023-04-04T21:27:15.819+0800    INFO    [init]  cmd/init.go:50  init app maudit success, client_id: t87ZhIzDiV6BZMs8qLRg4rZz, client_secret: GDqe0cG60MUD4RwEjQWWXfzOJFdP2Xmd
2023-04-04T21:27:15.821+0800    INFO    [init]  cmd/init.go:50  init app cmdb success, client_id: p2nMkW8FZGYLexZEDLtW1bOG, client_secret: SgU0tlA2BJWfqHJyistxlAWM0DdwLIGj
192:mcenter_gp gongpeng$ 

```
使用cmdb的客户端链接到注册中心，然后在链接到keyauth测试用例
![img_4.png](img%2Fimg_4.png)

比如，日志，trace，监控 一般不会放在业务层进行处理，认证也会使用中间件实现。中间件
多用于非业务，功能比较一致


中间件的实现原理(以restful为例)：主要处理req *Request, resp *Response。chain *FilterChain主要用来分析是否继续往下执行
```go
func (c CrossOriginResourceSharing) Filter(req *Request, resp *Response, chain *FilterChain) 
{
}
func (a *KeyauthAuther) RestfulAuthHandlerFunc(
    req *restful.Request,
    resp *restful.Response,
    chain *restful.FilterChain,
    ) {
    // 处理Request对象
    // *restful.Request
    
  
    
    // 处理完成后 需要直接中断返回
    // *restful.Response
    resp.Header()
    resp.Write()
    
    // 如果处理ok需要把一些中间结果 然后后面的请求也能方式, 需要把结果保存中 上下文中
    // 原生 c.Request.Context()
    // 如何:  context Get
    // 认证完成后, 我们需要用户名称或者其他信息 传递下去
    
    // Set Context
    req.SetAttribute("token", tk)
    req.Attribute("token").(*token.Token)
    
    // chain 用于将请求flow下去
    chain.ProcessFilter(req, resp)
    }

```
使用gin实现中间件，通过:路由.Use使用中间件
```go
func (a *KeyauthAuther) GinAuthHandlerFunc() gin.HandlerFunc {
	return func(c *gin.Context) {
    // 处理Request对象
    c.Request

    // 处理完成后 需要直接中断返回
    // 处理Response Header
    c.Writer.Header()
    // 处理Response Body
    c.Writer.Write()
    

    // 如果处理ok需要把一些中间结果 然后后面的请求也能方式, 需要把结果保存中 上下文中
    c.Request.Context()
    // 认证完成后, 我们需要用户名称或者其他信息 传递下去

    c.Set("username", "xxxx") // 向下传递参数
    c.Get("username")         // 获取参数

    // 把请求flow到下一站去处理，
    c.Next()
    return
	}
}
```
