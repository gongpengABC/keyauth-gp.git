测试用例的输出
```shell
/usr/local/go/bin/go tool test2json -t /private/var/folders/hy/r0c_qsrx1jz6c2_f331mvzwm0000gn/T/GoLand/___TestBookQuery_in_gitee_com_gongpengABC_keyauth_gp_client.test -test.v -test.paniconexit0 -test.run ^\QTestBookQuery\E$
=== RUN   TestBookQuery
2023-04-01 08:24:56	INFO	[mcenter resolver]	resolver/resolver.go:120	search application address: [{
  "Addr": "127.0.0.1:18050",
  "ServerName": "",
  "Attributes": null,
  "BalancerAttributes": null,
  "Type": 0,
  "Metadata": null
} {
  "Addr": "127.0.0.1:18050",
  "ServerName": "",
  "Attributes": null,
  "BalancerAttributes": null,
  "Type": 0,
  "Metadata": null
} {
  "Addr": "127.0.0.1:18050",
  "ServerName": "",
  "Attributes": null,
  "BalancerAttributes": null,
  "Type": 0,
  "Metadata": null
} {
  "Addr": "127.0.0.1:18050",
  "ServerName": "",
  "Attributes": null,
  "BalancerAttributes": null,
  "Type": 0,
  "Metadata": null
}]
access_token:"J4eRLNfqlXRd0Ae7igf2FlrT"  issue_at:1680216756353  data:{user_domain:"default"  user_name:"adminABC"}  access_token_expired_at:1680217356353  refresh_token:"akeuhkbzHpXznE4P4EDcSmDw"  refresh_token_expired_at:1680219756353
--- PASS: TestBookQuery (0.02s)
PASS

Process finished with the exit code 0
```
mcenter的输出:
```shell
    2023-04-01T08:24:56.039+0800    DEBUG   [instance]      impl/instance.go:89     
    search keyauth, 
    address: 
    [id:"bdbd3b4f" domain:"default" namespace:"default" 
     service_name:"keyauth" 
     registry_info:
     {region:"default" environment:"default" group:"default" 
      name:"ins-jkyvI4Vy" address:"127.0.0.1:18050" build:{}
     } 
     status:{online:1680098887725} 
     config:{heartbeat:{}} 
     id:"b6ffe8b1" domain:"default" 
     namespace:"default" service_name:"keyauth" 
     registry_info:
     {region:"default" environment:"default" group:"default" 
      name:"ins-3C7NwOUb" address:"127.0.0.1:18050" build:{}
     } 
     status:{online:1680129006221} config:{heartbeat:{}} 
     id:"de9dc7e9" domain:"default" namespace:"default" 
     service_name:"keyauth" 
     registry_info:
     {region:"default" environment:"default" group:"default" 
      name:"ins-nAz5WYxN" address:"127.0.0.1:18050" build:{}
     } 
     status:{online:1680264703422} 
     config:{heartbeat:{}} 
     id:"41e62a37" domain:"default" namespace:"default" 
     service_name:"keyauth" 
     registry_info:{region:"default" environment:"default" group:"default" 
     name:"ins-avpWoxwJ" address:"127.0.0.1:18050" build:{}} 
     status:{online:1680305756942} config:{heartbeat:{}}]
```