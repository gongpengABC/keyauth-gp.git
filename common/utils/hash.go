package utils

import "golang.org/x/crypto/bcrypt"

func HashPassword(password string) string {
	// 使用bcrypt对密码进行加密
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	if err != nil {
		panic(err)
	}
	return string(bytes)
}
func CheckPasswordHash(password, hash string) bool { // 即使是不同的字符串，解析出来的密码也可以是相同的
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
