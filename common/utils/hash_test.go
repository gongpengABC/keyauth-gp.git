package utils_test

import (
	"gitee.com/gongpengABC/keyauth-gp/common/utils"
	"testing"
)

func TestHashPassword(t *testing.T) {
	v := utils.HashPassword("abc")
	t.Log(v)
	ok := utils.CheckPasswordHash("abc", v)
	t.Log(ok)
	ok = utils.CheckPasswordHash("abc1", v)
	t.Log(ok)
}
