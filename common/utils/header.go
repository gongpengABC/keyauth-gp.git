package utils

import (
	"net/http"
	"strings"
)

// chatGPT参考代码
//func main() {
//	http.HandleFunc("/test", func(w http.ResponseWriter, r *http.Request) {
//		// 从HTTP请求头部获取Authorization字段的值
//		authHeader := r.Header.Get("Authorization")
//		if authHeader == "" {
//			w.WriteHeader(http.StatusBadRequest)
//			fmt.Fprintln(w, "Missing Authorization header")
//			return
//		}
//
//		// 解析Token
//		parts := strings.Split(authHeader, " ")
//		if len(parts) != 2 || strings.ToLower(parts[0]) != "bearer" {
//			w.WriteHeader(http.StatusBadRequest)
//			fmt.Fprintln(w, "Invalid Authorization header format")
//			return
//		}
//
//		token := parts[1]
//		fmt.Fprintln(w, "Token:", token)
//	})
//
//	http.ListenAndServe(":8080", nil)
//}

// 这里只需要返回一个string，不需要 w http.ResponseWriter

func GetToken(r *http.Request) (accessToken string) {
	auth := r.Header.Get("Authorization") // 获得token
	al := strings.Split(auth, " ")
	if len(al) > 1 {
		accessToken = al[1]
	} else {
		// 兼容 Authorization <token>，后面可能直接是token
		accessToken = auth
	}
	return
}
