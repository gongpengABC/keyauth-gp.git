package conf

import (
	"context"
	"fmt"
	"github.com/infraboard/mcenter/client/rpc"

	"sync"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	mgoclient *mongo.Client
)

func newConfig() *Config {
	return &Config{
		App: newDefaultAPP(),
		Log: newDefaultLog(),

		Mongo:   newDefaultMongoDB(),
		Mcenter: rpc.NewDefaultConfig(),
	}
}

// Config 应用配置
type Config struct {
	App *app `toml:"app"`
	Log *log `toml:"log"`

	Mongo *mongodb `toml:"mongodb"`
	// 注册中心的配置, 期望通过该配置能访问到注册中心
	// 通过 mcenter 通过的SDK(GRPC SDK Client) 来访问
	// 如何初始化 Mcenter GRPC Client ?
	// 通过 SDK 提供的LoadClientFromConfig来初始化的
	// 初始化后 mcenter 客户端包里面的全局变量 C来进行访问
	// rpc.C().Instance()
	// 后面 实现实例注册, 就执行使用 rpc.C() 这个全局对象
	Mcenter *rpc.Config `toml:"mcenter"`
}

/*
注册中心的配置, 期望通过该配置能访问到注册中心
通过 mcenter 通过的SDK(GRPC SDK Client) 来访问
如何初始化 Mcenter GRPC Client ?
通过 SDK 提供的LoadClientFromConfig来初始化的
初始化后 mcenter 客户端包里面的全局变量 C来进行访问
rpc.C().Instance()
后面 实现实例注册, 就执行使用 rpc.C() 这个全局对象


这是注册中心的代码。LoadClientFromConfig产生的客户端赋值一个全局变量，我们就可以直接调用.
所以rpc.C()是有值存在的。下面的InitGlobal()函数就直接调用了
var (
	client *ClientSet
)
func LoadClientFromConfig(conf *Config) error {
	c, err := NewClient(conf)
	if err != nil {
		return err
	}
	client = c
	return nil
}
*/

// mcenter的配置看作SDK服务

func (c *Config) InitGlobal() error {
	// 加载全局变量
	global = c
	// LoadClientFromConfig函数来自mcenter注册中心
	err := rpc.LoadClientFromConfig(c.Mcenter) // 加载好Mcenter的客户端
	if err != nil {
		return fmt.Errorf("load mcenter client from config error:" + err.Error())
	}
	rpc.C()
	return nil
}

type app struct {
	Name       string `toml:"name" env:"APP_NAME"`
	EncryptKey string `toml:"encrypt_key" env:"APP_ENCRYPT_KEY"`
	HTTP       *http  `toml:"http"`
	GRPC       *grpc  `toml:"grpc"`
}

func newDefaultAPP() *app {
	return &app{
		Name:       "cmdb",
		EncryptKey: "defualt app encrypt key",
		HTTP:       newDefaultHTTP(),
		GRPC:       newDefaultGRPC(),
	}
}

type http struct {
	Host      string `toml:"host" env:"HTTP_HOST"`
	Port      string `toml:"port" env:"HTTP_PORT"`
	EnableSSL bool   `toml:"enable_ssl" env:"HTTP_ENABLE_SSL"`
	CertFile  string `toml:"cert_file" env:"HTTP_CERT_FILE"`
	KeyFile   string `toml:"key_file" env:"HTTP_KEY_FILE"`
}

func (a *http) Addr() string {
	return a.Host + ":" + a.Port
}

func newDefaultHTTP() *http {
	return &http{
		Host: "127.0.0.1",
		Port: "8050",
	}
}

type grpc struct {
	Host      string `toml:"host" env:"GRPC_HOST"`
	Port      string `toml:"port" env:"GRPC_PORT"`
	EnableSSL bool   `toml:"enable_ssl" env:"GRPC_ENABLE_SSL"`
	CertFile  string `toml:"cert_file" env:"GRPC_CERT_FILE"`
	KeyFile   string `toml:"key_file" env:"GRPC_KEY_FILE"`
}

func (a *grpc) Addr() string {
	return a.Host + ":" + a.Port
}

func newDefaultGRPC() *grpc {
	return &grpc{
		Host: "127.0.0.1",
		Port: "18050",
	}
}

type log struct {
	Level   string    `toml:"level" env:"LOG_LEVEL"`
	PathDir string    `toml:"path_dir" env:"LOG_PATH_DIR"`
	Format  LogFormat `toml:"format" env:"LOG_FORMAT"`
	To      LogTo     `toml:"to" env:"LOG_TO"`
}

func newDefaultLog() *log {
	return &log{
		Level:   "debug",
		PathDir: "logs",
		Format:  "text",
		To:      "stdout",
	}
}

func newDefaultMongoDB() *mongodb {
	return &mongodb{
		Database:  "",
		Endpoints: []string{"127.0.0.1:27017"},
	}
}

type mongodb struct {
	Endpoints []string `toml:"endpoints" env:"MONGO_ENDPOINTS" envSeparator:","`
	UserName  string   `toml:"username" env:"MONGO_USERNAME"`
	Password  string   `toml:"password" env:"MONGO_PASSWORD"`
	Database  string   `toml:"database" env:"MONGO_DATABASE"`
	lock      sync.Mutex
}

// Client 获取一个全局的mongodb客户端连接
func (m *mongodb) Client() (*mongo.Client, error) {
	// 加载全局数据量单例
	m.lock.Lock()
	defer m.lock.Unlock()
	if mgoclient == nil {
		conn, err := m.getClient()
		if err != nil {
			return nil, err
		}
		mgoclient = conn
	}

	return mgoclient, nil
}

// GetDB 建立连接时，需要专门的数据库(m.Database).这里访问的数据库，就是用户认证的数据库
func (m *mongodb) GetDB() (*mongo.Database, error) {
	conn, err := m.Client()
	if err != nil {
		return nil, err
	}
	return conn.Database(m.Database), nil
}

func (m *mongodb) getClient() (*mongo.Client, error) {
	opts := options.Client()

	cred := options.Credential{
		AuthSource: m.Database, // 代表认证数据库，mongodb的用户的针对db。对应的用户和对应的库，一起创建
	}
	// 使用password进行认证
	if m.UserName != "" && m.Password != "" {
		cred.Username = m.UserName
		cred.Password = m.Password
		cred.PasswordSet = true
		opts.SetAuth(cred)
	}
	// mongodb的地址
	opts.SetHosts(m.Endpoints)
	opts.SetConnectTimeout(5 * time.Second)

	// Connect to MongoDB，惰性连接，没有真正连接
	client, err := mongo.Connect(context.TODO(), opts)
	if err != nil {
		return nil, fmt.Errorf("new mongodb client error, %s", err)
	}
	// 保证当前mongodb是在线的
	if err = client.Ping(context.TODO(), nil); err != nil {
		return nil, fmt.Errorf("ping mongodb server(%s) error, %s", m.Endpoints, err)
	}

	return client, nil
}
